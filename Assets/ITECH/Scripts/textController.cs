using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextController : MonoBehaviour
{
    public string nameController = "TextMeshPro-Controller";

    private MqttReceiver _eventSender;

    public string objectTopic;
    

    void Start()
    {   
        _eventSender = GameObject.FindGameObjectsWithTag("MQTT_R")[0].gameObject.GetComponent<MqttReceiver>();
        _eventSender.OnMessageArrived += OnMessageArrivedHandler;
    }

    private void OnMessageArrivedHandler(string topic, string newMsg)
    {
        if(topic == objectTopic){
            this.GetComponent<TextMeshProUGUI>().text=newMsg;
        }

        Debug.Log("Event Fired. The message, from Object " +nameController+" is = " + newMsg);
        Debug.Log("Event Topic: " + topic);
    }

}