using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHandler : MonoBehaviour
{

    private MqttReceiver _eventReceiver;


    [Tooltip("Set the topic to publish")]
    public string topicPublish = ""; // topic to publish

    public string actorState;

    void Start()
    {   
        _eventReceiver = GameObject.FindGameObjectsWithTag("MQTT_R")[0].gameObject.GetComponent<MqttReceiver>();
        _eventReceiver.OnMessageArrived += OnMessageArrivedHandler;
    }

    public void publishTopic(){
        
        string messagePublish = "";

        if(actorState == "True"){
            messagePublish = "False";
        }else{
            messagePublish = "True";
        }

        _eventReceiver.Publish(topicPublish, messagePublish);
    }

    private void OnMessageArrivedHandler(string topic, string newMsg)
    {
        if(topic == topicPublish){
            actorState=newMsg;
        }
    }
}
